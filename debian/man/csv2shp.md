csv2shp(1) -- converts a character delimited file to a ESRI shapefile
=====================================================================

##SYNOPSIS
`csv2shp` _csv_filename_ _delimiter_character_ _shp_filename_

        fprintf(stderr,
                "csv2shp csv_filename delimiter_character shp_filename\n");
        fprintf(stderr, "   csv_filename\n");
        fprintf(stderr,
                "     columns named longitude and latitude must exist\n");
        fprintf(stderr, "   delimiter_character\n");
        fprintf(stderr, "     one character only\n");
        fprintf(stderr, "   shp_filename\n");
        fprintf(stderr, "     base name, do not give the extension\n");
        return EXIT_FAILURE;


##DESCRIPTION
This program operates on single points only (not polygons or lines).

The input file may be a .csv file (comma separated values) or tab-separated values, or it may be separated by any other character.  The first row must contain column names.  There must be each a column named longitude and latitude in the input file.

The .csv parser does not understand text delimiters (e.g. quotation mark). It parses fields only by the given field delimiter (e.g. comma or tab). The program has not been tested with null values, and in this case, the behavior is undefined.  The program will not accept lines with a trailing delimiter character.

All columns (including longitude and latitude) in the input file are exported to the .dbf file.

The program attempts to find the best type (integer, decimal, string) and smallest size of the fields necessary for the .dbf file.

##OPTIONS
 * _csv_filename_:
 columns named longitude and latitude must exist

 * _delimiter_character_:
 one character only

 * _shp_filename_:
 base name, do not give the extension

